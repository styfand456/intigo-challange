import { URL_API } from '../constants/constants';
import Axios from 'axios';
import { createBrowserHistory } from 'history';
import { PATH_LOGIN } from '../../routes/path';
import { UNAUTHENTICATED } from '../../shared/strings/strings';
import { ValidatorForm } from 'react-material-ui-form-validator';
import {
  RULES_NAME_LENGHT_PASSWORD,
} from '../constants/validation';

export async function axiosApiService(endPoint, method, headers, data, callback, params = {}) {
  const config = {
    method: method,
    url: `${URL_API}${endPoint}`,
    timeout: 20000,
  };
  if (data) config.data = data;
  if (headers)
    config.headers = {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    };
  if (params)
    config.params = params;

  try {
    const response = await Axios(config);
    callback(undefined, response);
  } catch (error) {
    console.error(error);
    if (error.response.data.message === UNAUTHENTICATED) {
      localStorage.removeItem('token');
      const customHistory = createBrowserHistory();
      customHistory.go(PATH_LOGIN);
    }
    callback(error.response);
  }
}

export function lenghOfPassword() {
  ValidatorForm.addValidationRule(RULES_NAME_LENGHT_PASSWORD, (value) => {
    if (value.length < 8) {
      return false;
    }
    return true;
  });
}
