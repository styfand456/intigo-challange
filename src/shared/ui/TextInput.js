import React from 'react';
import './styles/textInput.scss';

const TextInput = props => {
    const { inputType, placeholder, value, onChange } = props;

    return (
        <div>
            <input className='intigo-auth-input' type={inputType} placeholder={placeholder} value={value} onChange={onChange} />
        </div>
    );
};

export default TextInput;