/**
 * File that contains all the strings used in this application
 */
export const LOGIN = 'Se connecter';
export const FORGOT_PASSWORD = 'Mot de passe oublié?';
export const BACK_TO_LOGIN_INTERFACE = "Retourner a la page d'accueil";
export const RESET_PASSWORD = 'Mot de passe oublié';
export const DONT_HAVE_ACCOUNT = "Vous n'avez pas de compte? S'inscrire";
export const REGISTER = "S'inscrire";
export const HAVE_AN_ACCOUNT = 'Vous avez déjà un compte? se connecter';
export const FIRST_NAME = 'Prenom';
export const LAST_NAME = 'Nom';
export const EMAIL = 'Email';
export const PHONE = 'Telephone';
export const PREFFESION = 'Profession';
export const ACTION = 'Action';
export const VALIDATE = 'valider';
export const PASSWORD = 'Mot de passe';
export const PASSWORD_REPEAT = 'Répéter le mot de passe';
export const FEMALE = 'Femme';
export const MALE = 'male';
export const EMAIL_EXISTS = 'Email existe';
export const EMAIL_PASSWORD_FAILED = 'Email ou mot de passe invalide';
export const AUTHOR = 'Sofiene Dimassi';
export const UNAUTHENTICATED = 'Unauthenticated.';
export const EMAIL_FAILED = "Ton adresse email n'est pas valide.";
export const EMAIL_SUCCESS = 'Merci de vérifier votre boite mail';
export const COUNTRY = 'Pays';
export const CITY = 'Ville';
export const REGION = 'Région';
export const TEMPERATURE_C = 'Température';
export const TEMPERATURE_F = 'Température';
export const PRESSURE = 'Pression';
export const WIND = 'Vent';
export const HUMIDITY = 'Humidité';
export const CLOUD = 'Nuage';
export const WEATHER = 'Météo';
