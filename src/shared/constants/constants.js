export const PRIMARY_COLOR = 'primary';
export const VARAINT_BODY_TWO = 'body2';
export const SECONDARY_COLOR = 'secondary';
export const INHERIT_COLOR = 'inherit';


export const CONTAINED = 'contained';

export const OUTLINED = 'outlined';

export const VARAINT_SUBTITLE_ONE = 'subtitle1';
export const VARAINT_SUBTITLE_TWO = 'subtitle2';
export const VARAINT_DETERMINATE = 'determinate';
export const TEXT_PRIMARY = 'textPrimary';
export const TEXT_SECONDARY = 'textSecondary';

export const GET = 'GET';

export const POST = 'POST';

export const PUT = 'PUT';

export const DELETE = 'DELETE';

export const URL_API = 'http://localhost:4042/api/';