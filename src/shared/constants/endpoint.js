export const ENDPOINT_REGISTER = 'users';
export const ENDPOINT_LOGIN = 'users/signin';
export const ENDPOINT_WEATHER = 'weather';