/**
 * File contains the validation message of form
 */

//Constant message : use when field is required
export const MESSAGE_VALIDATORS_REQUIRED = 'Ce champ est requis';

//Constant message : min length password
export const MESSAGE_VALIDATORS_PASSWORD =
  'Le mot de passe doit contenir au moins 8 caractères.';

// Constant message : email validation
export const MESSAGE_VALIDATORS_EMAIL = "L'email n'est pas valide";

// NAME OF RULES
export const RULES_NAME_LENGHT_PASSWORD = 'lenghPassword';
export const RULES_NAME_REQUIRED = 'required';
export const RULES_NAME_IS_EMAIL = 'isEmail';
