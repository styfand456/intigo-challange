import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useStyles } from './styles';
import { Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
/**
 * Component for showing Copyright at the end of each page
 */
export default function NotActivate() {
  const history = useHistory();
  const classes = useStyles(); //add styles to variable classes
  /**
   * Arrow function to disconnect the user when he clicks on the icon <ExitToAppIcon/>
   */
  const handleExitToAppIcon = () => {
    history.goBack();
  };
  return (
    <div className={classes.container}>
      <Typography className={classes.typoghraphy} align="center">
        Page Not Found
      </Typography>
      <Button style={{ color: 'white' }} onClick={handleExitToAppIcon}>
        Retour
      </Button>
    </div>
  );
}
