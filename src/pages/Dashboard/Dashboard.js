import { Box, Grid, Paper, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import Copyright from '../../components/Copyright/Copyright';
import { axiosApiService } from '../../shared/services/services';
import { useStyles } from './styles';
import * as constants from '../../shared/constants/constants';
import { ENDPOINT_WEATHER } from '../../shared/constants/endpoint';
import * as strings from '../../shared/strings/strings';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import CloudIcon from '@material-ui/icons/Cloud';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import { createBrowserHistory } from 'history';
import { PATH_LOGIN } from '../../routes/path';

/**
 * Component for showing dashboard Page
 */
export default function Dashboard() {
  const classes = useStyles(); //add styles to variable classes
  const [weather, setWeather] = useState();

  useEffect(() => {
    getCurrentWeather();
  }, []);

  const getCurrentWeather = () => {
    axiosApiService(
      ENDPOINT_WEATHER,
      constants.GET,
      true,
      null,
      onSuccess,
      { country: 'Tunisia' }
    );
  }

  const onSuccess = (error, response) => {
    error && console.error(error);
    if (response.data.success) {
      setWeather(response.data.weather);
    }
  };

  const logout = () => {
    localStorage.removeItem('token');
    const customHistory = createBrowserHistory();
    customHistory.go(PATH_LOGIN);
  };

  return (
    <Grid container className={classes.root}>
      <Grid item xs={false} sm={1} md={3} />
      <Grid item xs={12} sm={10} md={6} component={Paper} elevation={6} square>
        <div className={classes.container}>
          <Box className={classes.title} onClick={logout}>
            <Typography variant="h3"><b>{strings.WEATHER}</b></Typography>
          </Box>
          <Grid container className={classes.block}>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                {strings.COUNTRY}: <b>{weather?.location?.country}</b>
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                {strings.CITY}: <b>{weather?.location?.name}</b>
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.block}>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                <WbSunnyIcon fontSize="small" /> {strings.TEMPERATURE_C}: <b>{weather?.current?.temp_c}°C</b>
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                <AcUnitIcon fontSize="small" /> {strings.TEMPERATURE_F}: <b>{weather?.current?.temp_f}°F</b>
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.block}>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                {strings.PRESSURE}: <b>{weather?.current?.pressure_mb}°F</b>
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                {strings.WIND}: <b>{weather?.wind_kph}°F</b>
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.block}>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                <BubbleChartIcon fontSize="small" /> {strings.HUMIDITY}: <b>{weather?.humidity}°F</b>
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={6} >
              <Typography variant="h5">
                <CloudIcon fontSize="small" /> {strings.CLOUD}: <b>{weather?.cloud}°F</b>
              </Typography>
            </Grid>
          </Grid>
        </div>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Grid>
      <Grid item xs={false} sm={1} md={3} />
    </Grid>
  );
}
