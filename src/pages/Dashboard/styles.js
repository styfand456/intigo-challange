import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  container: {
    margin: theme.spacing(15, 2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  },
  title: {
    marginBottom: '40px',
  },
  avatar: {
    margin: theme.spacing(3),
    backgroundColor: 'yellow',
  },
  block: {
    margin: theme.spacing(3, 2),
  }
}));
