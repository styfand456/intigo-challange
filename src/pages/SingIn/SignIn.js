import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Alert from '@material-ui/lab/Alert';
import './signin.scss';
import { CircularProgress } from '@material-ui/core';
import React, { useState, useContext } from 'react';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { useHistory } from 'react-router-dom';
import Copyright from '../../components/Copyright/Copyright';
import * as strings from '../../shared/strings/strings';
import * as constants from '../../shared/constants/constants';
import * as validations from '../../shared/constants/validation';
import * as paths from '../../routes/path';
import { useStyles } from './styles';
import { IsActivateContext } from '../../shared/context/IsActivateContext';
import { axiosApiService } from '../../shared/services/services';
import { ENDPOINT_LOGIN } from '../../shared/constants/endpoint';

export default function SignIn() {
  const classes = useStyles(); //add styles to variable classes
  const history = useHistory(); //useHistory hook gives you access to the history instance that you may use to navigate.
  /**
   * The states used in this component
   * email : to retrieve the email entered by the user (initial value empty string)
   * password : to retrieve the password entered by the user (initial value empty string)
   * flag : state to generate an alert in case of invalid email or password (initial value false)
   */
  const [disabled, setDisabled] = useState(false);
  const [email, setEmail] = useState('');
  const [flag, setFlag] = useState(false);
  const [password, setPassword] = useState('');
  const { setIsActivate } = useContext(IsActivateContext);

  const handleEmail = (e) => {
    setEmail(e.target.value);
    setFlag(false);
  };

  const handlePassword = (e) => {
    setPassword(e.target.value);
    setFlag(false);
  };

  /**
   * arrow function to retrieve the final inputs
   * and call the funtion postLogin to send the data to the DB
   */
  const onSubmitForm = (e) => {
    setDisabled(true);
    e.preventDefault();
    const user = { email, password };
    axiosApiService(
      ENDPOINT_LOGIN,
      constants.POST,
      false,
      user,
      onSuccess
    );
  };

  const onSuccess = (error, response) => {
    error && console.error(error);
    if (!!response) {
      setDisabled(false);
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('status', response.data.status);
      setIsActivate(response.data.status === 1);
      history.push(paths.PATH_DASHBOARD);
    } else {
      setFlag(true);
      setDisabled(false);
    }
  };

  return (
    <div className='signin-body'>
      <div className='signin-body__container'>
        <div className='signin-body__container__icon'>
          <LockOutlinedIcon />
        </div>
        <div className='signin-body__container__title'>
          <h1>{strings.LOGIN}</h1>
        </div>
        {/* Alert when the user gives false data  */}
        {flag && (
          <Alert severity="error">{strings.EMAIL_PASSWORD_FAILED}</Alert>
        )}
        {/* Form */}
        <ValidatorForm onSubmit={onSubmitForm} className={classes.form}>
          <TextValidator
            label={strings.EMAIL}
            autoFocus
            autoComplete="email"
            onChange={handleEmail}
            value={email}
            validators={[
              validations.RULES_NAME_REQUIRED,
              validations.RULES_NAME_IS_EMAIL,
            ]}
            errorMessages={[
              validations.MESSAGE_VALIDATORS_REQUIRED,
              validations.MESSAGE_VALIDATORS_EMAIL,
            ]}
            fullWidth
            variant={constants.OUTLINED}
            margin="normal"
          />
          <TextValidator
            variant={constants.OUTLINED}
            label={strings.PASSWORD}
            type="password"
            onChange={handlePassword}
            value={password}
            validators={[validations.RULES_NAME_REQUIRED]}
            errorMessages={[validations.MESSAGE_VALIDATORS_REQUIRED]}
            fullWidth
          />
          <Button
            disabled={disabled}
            type="submit"
            fullWidth
            variant={constants.CONTAINED}
            color={constants.PRIMARY_COLOR}
            className={classes.submit}
          >
            {disabled && (
              <CircularProgress
                size={15}
                className={classes.circularProgress}
              />
            )}
            {strings.LOGIN}
          </Button>
        </ValidatorForm>
        <div className='signin-body__container__navigation'>
          <div>
            <Link
              href={paths.PATH_RESET_PASSWORD}
              variant={constants.VARAINT_BODY_TWO}
            >
              {strings.FORGOT_PASSWORD}
            </Link>
          </div>
          <div>
            <Link
              href={paths.PATH_REGISTER}
              variant={constants.VARAINT_BODY_TWO}
            >
              {strings.DONT_HAVE_ACCOUNT}
            </Link>
          </div>
        </div>
        <div className='signin-body__container__copyright'>
          <Copyright />
        </div>
      </div>
    </div>
  );
}
