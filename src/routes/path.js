export const PATH_LOGIN = '/';
export const PATH_REGISTER = '/register';
export const PATH_RESET_PASSWORD = '/resetPassword';
export const PATH_DASHBOARD = '/dashboard';
export const PATH_NOT_ACTIVATE = '/notActivate';
