import React, { Fragment, useContext } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import { CheckConnection } from '../components/CheckConnection/CheckConnection';
import { PrivateRoute } from '../components/PrivateRoute/PrivateRoute';
import * as paths from './path';
import NotActivate from '../pages/NotActivate/NotActivate';
import NotFound from '../pages/NotFound/NotFound';
import SignIn from '../pages/SingIn/SignIn';
import SignUp from '../pages/SignUp/SignUp';
import ForgotPassword from '../pages/ForgotPassword/ForgotPassword';
import { IsActivateContext } from '../shared/context/IsActivateContext';
import Dashboard from '../pages/Dashboard/Dashboard';

function Route() {
  const { isActivate } = useContext(IsActivateContext);

  return (
    <Fragment>
      <BrowserRouter>
        <Switch>
          {/* Components CheckConnection */}
          <CheckConnection exact path={paths.PATH_LOGIN} component={SignIn} />
          <CheckConnection path={paths.PATH_REGISTER} component={SignUp} />
          <CheckConnection
            exact
            path={paths.PATH_RESET_PASSWORD}
            component={ForgotPassword}
          />

          <PrivateRoute
            exact
            path={`${paths.PATH_NOT_ACTIVATE}`}
            component={NotActivate}
          />
          <PrivateRoute
            exact
            path={paths.PATH_DASHBOARD}
            component={() =>
              isActivate ? (
                <Dashboard />
              ) : (
                <NotActivate />
              )
            }
          />
          <PrivateRoute component={NotFound} />
        </Switch>
      </BrowserRouter>
    </Fragment>
  );
}

export default Route;
