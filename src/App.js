import React, { useState } from 'react';
import './App.scss';
import Route from './routes/Route';
import { IsActivateContext } from './shared/context/IsActivateContext';

function App() {
  const [isActivate, setIsActivate] = useState(
    localStorage.getItem('status') === '1'
  );

  return (
    <div className="App">
      <IsActivateContext.Provider value={{ isActivate, setIsActivate }}>
        <Route />
      </IsActivateContext.Provider>
    </div>
  );
}

export default App;
