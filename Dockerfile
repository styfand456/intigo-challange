### STAGE 1: Build ###

# pull official node image
FROM node:14-alpine as node

## The one to blame
LABEL maintainer="Sofiene DIMASSI <dimessisofiene@gmail.com>"

## How to run this file
LABEL cmd="docker build -t <image name> . (sudo permission required for mac/linux)"
LABEL cmd="docker run -d -p 3000:80 <image name> . (sudo permission required for mac/linux)"


# define working directory
WORKDIR /app

# copy /package.json to working directory
COPY ./package.json .

# install app dependencies
RUN npm install

# copy project to working directory
COPY . .

# install app dependencies
RUN npm run build

### STAGE 2: Setup ###

# pull official nginx image
FROM nginx:alpine

# copy the build file generated in the first stage
COPY --from=node /app/build /usr/share/nginx/html/intigo-front

# copy nginx's config file
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

## Ps: this image size is 25.4MB ##